variable "product" {}
variable "region" {}
variable "env" {}
variable "stage" {}
variable "app_name" {}
variable "artifact_bucket_name" {}
variable "source_repository_url" {
  description = "The owner and name of the repository where source changes are to be detected"
}
variable "vpc_config" {
  type = object({
    vpcId      = string
    subnetIds = list(string)
    securityGroupIds = list(string)
  })
}
variable "aws_account" {}

provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12.0"
  backend "s3" {
    encrypt = true
  }
}

module "bootstrap" {
  source                              = "./bootstrap"
  codebuild_iam_role_name             = "${var.app_name}-codebuild-role"
  codebuild_iam_role_policy_name      = "${var.app_name}-codebuild-policy"
  codepipeline_iam_role_name          = "${var.app_name}-codepipeline-role"
  codepipeline_iam_role_policy_name   = "${var.app_name}-codepipeline-policy"
}

module "codebuild" {
  source                                  = "./codebuild"
  product                                 = var.product
  environment                             = var.env
  stage                                   = var.stage
  codebuild_image                         = "${var.aws_account}.dkr.ecr.${var.region}.amazonaws.com/csc/node-terraform-kube:1.0.1"
  codebuild_project_migration_revert_name = "${var.app_name}-MigrateRevert"
  codebuild_project_migration_run_name    = "${var.app_name}-MigrateRun"
  codebuild_iam_role_arn                  = module.bootstrap.codebuild_iam_role_arn
  vpc_config                              = var.vpc_config
}

module "codepipeline" {
  source                             = "./codepipeline"
  product                            = var.product
  stage                              = var.stage
  codepipeline_source_repository_url = var.source_repository_url
  codepipeline_name                  = "${var.app_name}-codepipeline"
  codepipeline_iam_role_arn          = module.bootstrap.codepipeline_iam_role_arn
  codepipeline_artifact_bucket_name  = var.artifact_bucket_name
  codestar_connection_arn            = "arn:aws:codestar-connections:us-east-1:929226109038:connection/45f43276-a22f-47ec-9696-e3b72499e450"
  codebuild_migration_revert_name    = module.codebuild.codebuild_migration_revert_name
  codebuild_migration_run_name       = module.codebuild.codebuild_migration_run_name
}