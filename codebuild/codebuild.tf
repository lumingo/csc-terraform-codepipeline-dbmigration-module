resource "aws_codebuild_project" "codebuild_project_migration_revert" {
  name          = var.codebuild_project_migration_revert_name
  description   = "Terraform codebuild project"
  build_timeout = "5"
  service_role  = var.codebuild_iam_role_arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = var.codebuild_image
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    environment_variable {
      name  = "ENV"
      value = var.environment
    }
    environment_variable {
      name  = "ENV_TAG"
      value = var.stage
    }
  }

  vpc_config {
    vpc_id = var.vpc_config.vpcId
    subnets = var.vpc_config.subnetIds
    security_group_ids = var.vpc_config.securityGroupIds
  }

  source {
    type      = "CODEPIPELINE"
    buildspec = "buildspec_migration_revert.yml"
  }

  tags = {
    Environment = var.stage
    Product = var.product
  }
}

# Output TF Plan CodeBuild name to main.tf
output "codebuild_migration_revert_name" {
  value = var.codebuild_project_migration_revert_name
}


# Create CodeBuild Project for Terraform Apply
resource "aws_codebuild_project" "codebuild_project_migration_run" {
  name          = var.codebuild_project_migration_run_name
  description   = "Codebuild project"
  build_timeout = "5"
  service_role  = var.codebuild_iam_role_arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = var.codebuild_image
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    environment_variable {
      name  = "ENV"
      value = var.environment
    }
    environment_variable {
      name  = "ENV_TAG"
      value = var.stage
    }
  }

  vpc_config {
    vpc_id = var.vpc_config.vpcId
    subnets = var.vpc_config.subnetIds
    security_group_ids = var.vpc_config.securityGroupIds
  }

  source {
    type      = "CODEPIPELINE"
    buildspec = "buildspec_migration_run.yml"
  }

  tags = {
    Environment = var.stage
    Product = var.product
  }
}

# Output TF Plan CodeBuild name to main.tf
output "codebuild_migration_run_name" {
  value = var.codebuild_project_migration_run_name
}
