variable "product" {
  default = "csc"
}

variable "environment" {
  description = "Pass environment"
}
variable "stage" {
  description = "Environment name"
}
variable "codebuild_image" {
  description = "Docker image used from CodeBuild"
}
variable "codebuild_project_migration_revert_name" {
  description = "Name for CodeBuild Terraform Plan Project"
}
variable "codebuild_project_migration_run_name" {
  description = "Name for CodeBuild Terraform Apply Project"
}
variable "codebuild_iam_role_arn" {
  description = "ARN of the CodeBuild IAM role"
}
variable "vpc_config" {
    type = object({
      vpcId      = string
      subnetIds = list(string)
      securityGroupIds = list(string)
    })
    description = "VPC Config"
}
