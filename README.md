# Terraform CodePipeline Module

Modulo terraform para creación de un pipeline de migración de base de datos en AWS

## Usage

Agregar módulo el proyecto

```
module "pipeline-dbmigration" {
  source                                  = "bitbucket.org/orbisunt/csc-terraform-codepipeline-dbmigration-module"
  aws_account                             = var.aws_account
  product                                 = var.product
  env                                     = var.env
  region                                  = var.region
  stage                                   = var.stage
  app_name                                = var.app_name
  vpc_config                              = var.vpc_config
  artifact_bucket_name                    = var.artifact_bucket_name
  source_repository_url                   = var.source_repository_url
}
```

El repositorio que use llame al módulo debe contar con los archivos Buildspec de ejecución y reversa de migración

   * buildspec_migration_revert.yml
   * buildspec_migration_run.yml