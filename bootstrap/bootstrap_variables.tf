variable "codebuild_iam_role_name" {
  description = "Name for IAM Role utilized by CodeBuild"
}
variable "codebuild_iam_role_policy_name" {
  description = "Name for IAM policy used by CodeBuild"
}
variable "codepipeline_iam_role_name" {
  description = "Name of the Terraform CodePipeline IAM Role"
}
variable "codepipeline_iam_role_policy_name" {
  description = "Name of the Terraform IAM Role Policy"
}