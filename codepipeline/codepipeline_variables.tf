variable "product" {
  default = "csc"
}

variable "stage" {
  default = "development"
}

variable "codepipeline_source_repository_url" {
  description = "GIT Repository URL for CodePipeline source"
}

variable "codepipeline_artifact_bucket_name" {
  description = "Name of the CodePipeline S3 bucket for artifacts"
}
variable "codepipeline_name" {
  description = "CodePipeline Name"
}
variable "codepipeline_iam_role_arn" {
  description = "Role codepipeline"
}
variable "codebuild_migration_revert_name" {
  description = "Migration RUN codebuild project name"
}
variable "codebuild_migration_run_name" {
  description = "Migration REVERT codebuild project name"
}
variable "codestar_connection_arn" {
  description = "ARN CodeStar to connect BitBucket"
}