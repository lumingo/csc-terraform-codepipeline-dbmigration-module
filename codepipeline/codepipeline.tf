resource "aws_codepipeline" "codepipeline-migration-run" {
  name     = "${var.codepipeline_name}-migration-run"
  role_arn = var.codepipeline_iam_role_arn

  artifact_store {
    location = var.codepipeline_artifact_bucket_name
    type     = "S3"
  }

  stage {
    name = "Source"
    action {
      name             = "ApplicationSource"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["SourceArtifact"]
      configuration = {
        ConnectionArn     = var.codestar_connection_arn
        FullRepositoryId  = var.codepipeline_source_repository_url
        BranchName        = "master"
        DetectChanges     = false
      }
    }
  }

  stage {
    name = "MigrationRun"

    action {
      name            = "Migration-Run"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["SourceArtifact"]
      version         = "1"

      configuration = {
        ProjectName = var.codebuild_migration_run_name
      }
    }
  }

  tags = {
    Environment = var.stage
    Product = var.product
  }
}

resource "aws_codepipeline" "codepipeline-migration-revert" {
  name     = "${var.codepipeline_name}-migration-revert"
  role_arn = var.codepipeline_iam_role_arn

  artifact_store {
    location = var.codepipeline_artifact_bucket_name
    type     = "S3"
  }

  stage {
    name = "Source"
    action {
      name             = "ApplicationSource"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["SourceArtifact"]
      configuration = {
        ConnectionArn     = var.codestar_connection_arn
        FullRepositoryId  = var.codepipeline_source_repository_url
        BranchName        = "master"
        DetectChanges     = false
      }
    }
  }

  stage {
    name = "MigrationRevert"

    action {
      name             = "Migrations-Revert"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["SourceArtifact"]
      version          = "1"

      configuration = {
        ProjectName = var.codebuild_migration_revert_name
      }
    }
  }

  tags = {
    Environment = var.stage
    Product = var.product
  }
}
