variable "product" {
    default = "csc"
}

variable "stage" {
    default = "development"
}

variable "name" {
    description = "CodeStar name"
}