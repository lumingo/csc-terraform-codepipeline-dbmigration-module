resource "aws_codestarconnections_connection" "tf_codestar" {
  name          = var.name
  provider_type = "Bitbucket"
  tags = {
    Environment = var.stage
    Product = var.product
  }
}

output "codestar_connection_arn" {
  value = aws_codestarconnections_connection.tf_codestar.arn
}